//
//  PantryServiceApp.swift
//  PantryService
//
//  Created by Merin K Thomas on 06/03/23.
//

import SwiftUI

@main
struct PantryServiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
